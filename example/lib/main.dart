import 'package:animated_emerge/animated_emerge.dart';
import 'package:flutter/material.dart';
import 'package:flutter_hooks/flutter_hooks.dart';

void main() {
  runApp(const App());
}

class App extends StatelessWidget {
  const App({super.key});

  @override
  Widget build(BuildContext context) {
    final visible = useState(false);

    return Material(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          AnimatedEmerge(
            emerged: visible.value,
            child: SizedBox.square(
              dimension: 100,
              child: ClipOval(
                child: DecoratedBox(
                  decoration: BoxDecoration(
                    color: Colors.blue.shade500,
                  )
                ),
              ),
            )
          ),
          const SizedBox(height: 20),
          TextButton(
            onPressed: () => visible.value = false,
            child: const Text("Toggle"),
          )
        ],
      ),
    );
  }
}
