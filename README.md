AnimatedEmerge simply animates the visibility of a widget.

Usage:
```dart
class App extends StatelessWidget {
  const App({super.key});

  @override
  Widget build(BuildContext context) {
    return AnimatedEmerge(
      emerged: true, // toggle this to change the visibility of the child widget
      child: SizedBox.square( // the child widget can of course be anything
        dimension: 100,
        child: ClipOval(
          child: DecoratedBox(
            decoration: BoxDecoration(
              color: Colors.blue.shade500,
            )
          ),
        ),
      )
    );
  }
}
```
