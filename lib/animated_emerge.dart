import 'package:flutter/widgets.dart';

enum EmergeBehaviour {
  size,
  scale,
}

enum EmergeAxis { horizontal, vertical, both }

class AnimatedEmerge extends StatefulWidget {
  final bool emerged;
  final Duration duration;
  final Duration? emergeDuration;
  final Duration? vanishDuration;
  final Curve curve;
  final Curve? emergeCurve;
  final Curve? vanishCurve;
  final EmergeBehaviour emergeBehaviour;
  final EmergeAxis emergeAxis;
  final Widget child;

  const AnimatedEmerge({
    super.key,
    this.emerged = true,
    this.duration = const Duration(milliseconds: 300),
    this.emergeDuration,
    this.vanishDuration,
    this.curve = Curves.easeInOut,
    this.emergeCurve,
    this.vanishCurve,
    this.emergeBehaviour = EmergeBehaviour.size,
    this.emergeAxis = EmergeAxis.both,
    required this.child,
  });

  @override
  State<StatefulWidget> createState() => AnimatedEmergeState();
}

class AnimatedEmergeState extends State<AnimatedEmerge>
    with TickerProviderStateMixin {
  late AnimationController sizeAnimationController;
  late AnimationController fadeAnimationController;

  final GlobalKey offstageChild = GlobalKey();

  @override
  void initState() {
    super.initState();
    sizeAnimationController = AnimationController(
      vsync: this,
      value: _animationTarget,
    )..addListener(() => setState(() {}));
    fadeAnimationController = AnimationController(
      vsync: this,
      value: _animationTarget,
    )..addListener(() => setState(() {}));
  }

  bool get isActive {
    return sizeAnimationController.value != 0 ||
        fadeAnimationController.value != 0;
  }

  Curve get _evaluatedSizeCurve {
    if (widget.emerged) {
      return widget.emergeCurve ?? widget.curve;
    } else {
      return widget.vanishCurve ?? widget.curve;
    }
  }

  Duration get _evaluatedDuration {
    if (widget.emerged) {
      return widget.emergeDuration ?? widget.duration;
    } else {
      return widget.vanishDuration ?? widget.duration;
    }
  }

  @override
  void didUpdateWidget(covariant AnimatedEmerge oldWidget) {
    if (oldWidget.emerged != widget.emerged) {
      sizeAnimationController.animateTo(_animationTarget,
          curve: _evaluatedSizeCurve, duration: _evaluatedDuration);
      fadeAnimationController.animateTo(
        _animationTarget,
        curve: Curves.easeInOut,
        duration: _evaluatedDuration,
      );
    }
    super.didUpdateWidget(oldWidget);
  }

  Size? get _preferredSize {
    final childRenderBox =
        offstageChild.currentContext?.findRenderObject() as RenderBox?;
    return childRenderBox?.size;
  }

  Size? get _displaySize {
    final preferredSizeCached = _preferredSize ?? Size.zero;
    final beginSize = Size(
        widget.emergeAxis == EmergeAxis.horizontal
            ? 0
            : preferredSizeCached.width,
        widget.emergeAxis == EmergeAxis.vertical
            ? 0
            : preferredSizeCached.height);
    return SizeTween(
      begin: beginSize,
      end: preferredSizeCached,
    ).evaluate(sizeAnimationController);
  }

  double get _animationTarget {
    return widget.emerged ? 1.0 : 0.0;
  }

  double get _fadeAmount {
    return fadeAnimationController.value;
  }

  @override
  Widget build(BuildContext context) {
    // If all animation controllers are at value 1.0 we can get rid of the part
    // of the tree that is responsible for the emerge behaviour.
    // This conveniently also resolves an initial-build-problem, where the
    // offstage widget has not yet laid out its child so that we dont no the
    // preferredSize, and thus can't use the emerge behaviour part of the tree.
    final showNormal = fadeAnimationController.value == 1.0 &&
        sizeAnimationController.value == 1.0;
    // If one of the animation controllers has a non-zero value we need to
    // include the child in the (rendered, excluding offstage) widget tree.
    final showAtAll = fadeAnimationController.value != 0.0 ||
        sizeAnimationController.value != 0.0;

    return Stack(
      fit: StackFit.passthrough,
      children: [
        Offstage(
          offstage: true,
          child: Container(key: offstageChild, child: widget.child),
        ),
        if (showNormal && showAtAll) widget.child,
        if (!showNormal && showAtAll)
          SizedBox.fromSize(
            size: _displaySize,
            child: Opacity(
              opacity: _fadeAmount,
              child: _buildClient(context),
            ),
          )
      ],
    );
  }

  Widget _buildClient(BuildContext context) {
    switch (widget.emergeBehaviour) {
      case EmergeBehaviour.size:
        return _buildSizedClient(context);
      case EmergeBehaviour.scale:
        return _buildScaledClient(context);
    }
  }

  Widget _buildSizedClient(BuildContext context) {
    return widget.child;
  }

  /// Scales its client widget to fit the display size while rendering it with
  /// its preferred size
  Widget _buildScaledClient(BuildContext context) {
    final pretendedSize = _preferredSize ?? Size.zero;
    final actualSize = _displaySize ?? Size.zero;
    final scaleX = actualSize.width / pretendedSize.width;
    final scaleY = actualSize.height / pretendedSize.height;
    return OverflowBox(
      minWidth: pretendedSize.width,
      maxWidth: pretendedSize.width,
      minHeight: pretendedSize.height,
      maxHeight: pretendedSize.height,
      child: Transform.scale(
        scaleX: scaleX,
        scaleY: scaleY,
        child: widget.child,
      ),
    );
  }

  @override
  void dispose() {
    sizeAnimationController.dispose();
    fadeAnimationController.dispose();
    super.dispose();
  }
}
